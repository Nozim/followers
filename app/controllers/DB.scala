package controllers

import com.ibm.couchdb.{CouchDb, TypeMapping}

case class User(username: String, password: String)
object DB {
  val typeMapping = TypeMapping(classOf[User] -> "User")

  def setup = {
    val couch = CouchDb("dockerhost", 5984)
    val db = couch.db("users",typeMapping)
    val u = User("foo","bar")
    db.docs.create(u)
    println(db.docs)
    db
  }


}
