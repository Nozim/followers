package controllers

import javax.inject._

import play.api.data.Form
import play.api.data.Forms._
import play.api.mvc._


@Singleton
class HomeController @Inject() extends Controller {

val dbName = "users"
  def signupForm = Form(mapping("username"-> nonEmptyText, "password"-> nonEmptyText)(User.apply)(User.unapply))
  def signup = Action { implicit request => {

    val  db = DB.setup
    if (request.method == "GET") {
      Ok(views.html.signup())
    }
    else {
      signupForm.bindFromRequest().fold (
        formWithErrors => {
          BadRequest("invalid creds")
        },
          user => {
            val db = DB.setup
            db.docs.create(user)
            Ok(views.html.index("you have signed up")).withSession(("username",user.username))
          }
      )
      Ok(views.html.index(" "))

    }
  }
  }


  def login = Action { implicit request => {
    if (request.method == "GET") {
      Ok(views.html.signup())
    }
    else {

      signupForm.bindFromRequest().fold(
        formWithErrors => {
          BadRequest("invalid creds")
        },
        user => {
//          val couch = CouchDb("dockerhost", 5984)
//          val db = couch.db("users",typeMapping)
//          val actions = for {
//          // Delete the database or ignore the error if it doesn't exist
//            _ <- couch.dbs.delete(dbName).ignoreError
//            // Create a new database
//            _ <- couch.dbs.create(dbName)
//            // Insert documents into the database
//            // Retrieve all documents from the database and unserialize to Person
//            docs <- db.docs.getMany.queryIncludeDocs[User]
//          } yield docs.getDocsData
          Ok(views.html.index("Please login first"))
        })

    }}}



  def index = Action { request => {
    DB.setup
    Ok(views.html.index("Please login first"))
  }
  }
}
